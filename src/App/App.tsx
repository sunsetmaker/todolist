
import styles from './App.module.css'

import {
  ChangeEvent,
  FC,
  Reducer,
  useReducer,
  useState,
} from 'react'

import { TodoItem } from '../TodoItem/TodoItem'

interface ITodo {
  isCompleted: boolean
  title: string
}

interface ITodoState {
  todos: ITodo[]
}

interface ITodoAddAction {
  type: 'ADD_TODO'
  payload: ITodo
}

interface ITodoToggleAction {
  type: 'TOGGLE_TODO'
  payload: number
}

interface ITodoDestroyAction {
  type: 'DESTROY_TODO'
  payload: number
}

type ITodoAction = ITodoAddAction | ITodoToggleAction | ITodoDestroyAction

const MIN_TITLE_LENGTH = 3

const initialState = {
  todos: []
}

const todoReducer = (state: ITodoState, action: ITodoAction) => {
  switch (action.type) {
    case 'ADD_TODO':
      return {
        todos: [...state.todos, action.payload],
      }
    case 'TOGGLE_TODO':
      return {
        todos: state.todos.map((todo, index) => {
          if (index === action.payload) {
            return {
              ...todo,
              isCompleted: !todo.isCompleted,
            }
          }
          return todo
        }),
      }
    case 'DESTROY_TODO':
      return {
        todos: [...state.todos.slice(0, action.payload), ...state.todos.slice(action.payload + 1)],
      }
    default:
      return state
  }
}

const App: FC = () => {
  const [{ todos }, dispatch] = useReducer<Reducer<ITodoState, ITodoAction>>(todoReducer, initialState)
  const [value, setValue] = useState('')

  const handleAddTodo = (title: string) => {
    dispatch({
      type: 'ADD_TODO',
      payload: { title, isCompleted: false },
    })
  }

  const onToggle = (index: number) => {
    dispatch({
      type: 'TOGGLE_TODO',
      payload: index,
    })
  }

  const onDestroy = (index: number) => {
    dispatch({
      type: 'DESTROY_TODO',
      payload: index,
    })
  }

  const onSubmit = (event: ChangeEvent<HTMLFormElement>): void => {
    event.preventDefault()

    if (value.length >= MIN_TITLE_LENGTH) {
      handleAddTodo(value)
      setValue('')
    }
  }

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValue(event.currentTarget.value)
  }

  console.log('todos', todos)

  return (
    <div className={styles.root}>
      <h1>Hello CodeSandbox</h1>
      <ul className={styles.list}>
        {todos.map((todo: ITodo, index: number) => (
          <li key={index}>
            <TodoItem
              index={index}
              isCompleted={todo.isCompleted}
              onDestroy={onDestroy}
              onToggle={onToggle}
              title={todo.title}
            />
          </li>
        ))}
      </ul>

      <form onSubmit={onSubmit}>
        <input type='text' onChange={onChange} value={value} />
        <button type='submit'>Add Todo</button>
      </form>
    </div>
  )
}

export default App
