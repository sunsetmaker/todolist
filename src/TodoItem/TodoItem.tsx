import { FC } from 'react'
interface ITodoItem {
  index: number
  isCompleted: boolean
  onDestroy: (index: number) => void
  onToggle: (index: number) => void
  title: string
}
export const TodoItem: FC<ITodoItem> = ({
  index,
  isCompleted,
  onDestroy,
  onToggle,
  title,
}) => {
  return (
    <div>
      <input
        defaultChecked={isCompleted}
        onClick={() => onToggle(index)}
        type='checkbox'
      />
      {title}
      <button
        type='button'
        onClick={() => onDestroy(index)}
      >
        x
      </button>
    </div>
  )
}
